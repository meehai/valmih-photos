<?php
require_once("album.php"); 

class photos extends Module {
	function show() {}

	function install() {
		$this->connection->createTable("photos_album", ["id", "authorId", "albumTitle"],
			["integer", "integer", "varchar(50)"]);
        $this->connection->createTable("photos", ["id", "albumId", "photoFile", "photoTitle", "photoDescription"],
            ["integer", "integer", "varchar(50)", "varchar(50)", "varchar(50)"]);
    }

    function createAlbum($userName, $albumTitle) {
        $album = Album::createAlbum($this->connection, $userName, $albumTitle);
        $albumId = $album->albumId;
        echo "Album $albumTitle (ID: $albumId) created succesfully";
        return $albumId;
    }

    function loadAlbumFromName(string $authorName, string $albumTitle) {
        return Album::loadAlbumFromName($this->connection, $authorName, $albumTitle);
    }

    function loadAlbum($albumId) {
        try {
            return new Album($this->connection, $albumId);
        }
        catch (Exception $e) {
            return null;
        }
    }

    function loadUserAlbums(string $authorName) {
        return Album::loadUserAlbums($this->connection, $authorName);
    }

	function getOnePhoto($id) {
		$res = $this->connection->query("SELECT * FROM photos where id='$id'");
		$res = $res->fetchArray("assoc");
        assert (count($res) == 1);
        return $res[0];
	}


}

?>