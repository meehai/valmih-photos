<?php

function make_thumb($src, $dest, $desired_width) {
	/* read the source image */
	$source_image = imagecreatefromjpeg($src);
	$width = imagesx($source_image);
	$height = imagesy($source_image);

	/* find the "desired height" of this thumbnail, relative to the desired width  */
	$desired_height = floor($height * ($desired_width / $width));

	/* create a new, "virtual" image */
	$virtual_image = imagecreatetruecolor($desired_width, $desired_height);

	/* copy source image at a resized size */
	imagecopyresampled($virtual_image, $source_image, 0, 0, 0, 0, $desired_width, $desired_height, $width, $height);

	/* create the physical thumbnail image to its destination */
	imagejpeg($virtual_image, $dest);
}

class Album {
	function __construct($connection, $albumId) {
		$this->connection = $connection;
		$this->checkAlbum($albumId);
		$this->albumId = $albumId;
		/* Load album data */
		$this->albumTitle = $this->loadAlbumTitle();
		$this->authorName = $this->getAuthorName();
		// $this->photos = $this->getAlbumPhotos();
	}

	private function getAuthorName() {
		$albumId = $this->albumId;
		$res = $this->connection->query("SELECT authorId FROM photos_album where id='$albumId'");
		$authorId = $res->fetchArray("assoc")[0]["authorId"];
		assert (!$name == NULL);
		$res = $this->connection->query("SELECT username FROM users where id='$authorId'");
		$name = $res->fetchArray("assoc")[0]["username"];
		return $name;
	}

	private function loadAlbumTitle() {
		$albumId = $this->albumId;
		$res = $this->connection->query("SELECT albumTitle FROM photos_album where id='$albumId'");
		$title = $res->fetchArray("assoc")[0]["albumTitle"];
		assert (!$title == NULL);
		return $title;
	}

	private function checkAlbum(int $albumId) {
		$res = $this->connection->query("SELECT albumTitle from photos_album where id='$albumId'");
		$items = $res->fetchArray("num");
		if (count($items) != 1) {
			throw new Exception("Wrong album id");
		}
	}

	public static function loadUserAlbums($connection, string $authorName) {
		$authorId = $connection->getUserID($authorName);
		$res = $connection->query("SELECT id FROM photos_album where authorId='$authorId'");
		$albumIds = $res->fetchArray("assoc");
		if ($albumIds == NULL) {
			return [];
		}

		$albums = [];
		foreach ($albumIds as $id) {
			$albums[] = new Album($connection, $id["id"]);
		}
		return $albums;
	}

	public function getAlbumPhotos() {
		$res = $this->connection->query("SELECT * FROM photos where albumId='{$this->albumId}'");
		$res = $res->fetchArray("assoc");
		/* Create thumbnails for image presentation */
		for($i=0; $i<count($res); $i++) {
			$this->createThumbnail($res[$i]["photoFile"]);
			$res[$i]["thumbnailFile"] = "thumbnails/" . $res[$i]["photoFile"];
		}
		return $res;
	}

	private function createThumbnail($img) {
		$albumPath = Constants::$absolutePath . "/public_files/photos/{$this->albumId}";
		$thumbnailDirPath = "$albumPath/thumbnails";
		if(!file_exists($thumbnailDirPath)) {
			mkdir($thumbnailDirPath);
		}

		$imagePath = "$albumPath/$img";
		$thumbnailPath = "$thumbnailDirPath/$img";
		if(!file_exists($thumbnailPath)) {
			make_thumb($imagePath, $thumbnailPath, 300);
		}
	}

}

?>